package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gotest4/restApi/api/exceptions"
	"gotest4/restApi/api/models"
	"gotest4/restApi/api/services"
)

func Topics(router *gin.Engine) {

	router.GET("/topics", getTopics)
}

func getTopics(context *gin.Context) {

	topics, err := services.GetTopics()

	if err == 0 {

		context.IndentedJSON(http.StatusOK, topics)
		
	} else {

		getError(context, err)
	}

}

func getError(context *gin.Context, err int) {

	switch err {

	case 500:

		var errorModel models.Error
		errorModel.Code = err
		errorModel.Message = exceptions.InternalServerError
		
		context.IndentedJSON(http.StatusInternalServerError, errorModel)

	case 503:
		
		var errorModel models.Error
		errorModel.Code = err
		errorModel.Message = exceptions.ServiceUnavailable
		
		context.IndentedJSON(http.StatusServiceUnavailable, errorModel)
	}
}