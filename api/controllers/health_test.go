package controllers_test

import (
	"gotest4/restApi/api/controllers"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetHealth(t *testing.T) {

	router := setGin()

	controllers.Health(router)

	req, _ := http.NewRequest("GET", "/health", nil)
    w := httptest.NewRecorder()
    router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
}

// func setGin() (*gin.Engine) {

// 	router := gin.Default()

// 	return router
// }