package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func Health(router *gin.Engine) {

	router.GET("/health", getHealth)
}

func getHealth(context *gin.Context) {

	context.JSON(http.StatusOK, gin.H{"message": "OK"})
}