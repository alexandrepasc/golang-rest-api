package controllers_test

import (
	"encoding/json"
	"gotest4/restApi/api/controllers"
	"gotest4/restApi/api/models"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
	"github.com/xeipuuv/gojsonschema"
)

func TestGetError(t *testing.T) {

	const schemaFile = "topics_service_unavailable_schema.json"
	const responseCode = 503

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	mockResponse(t, "topics_error_response.json", 500)

	router := setGin()

	controllers.Topics(router)

	req, _ := http.NewRequest("GET", "/topics", nil)
    w := httptest.NewRecorder()
    router.ServeHTTP(w, req)

	responseData, _ := ioutil.ReadAll(w.Body)
	var response models.Error
	json.Unmarshal(responseData, &response);

	assert.Equal(t, responseCode, w.Code)
	
	responseSchema := getSchema(schemaFile)
	responseLoader := gojsonschema.NewGoLoader(response)

	result, validationError := gojsonschema.Validate(responseSchema, responseLoader)

	if validationError != nil {
        t.Errorf(validationError.Error())
    }

	assert.True(t, result.Valid(), response)
}

func TestValidResponse(t *testing.T) {

	const schemaFile = "topics_valid_response_schema.json"
	const responseCode = 200

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	mockResponse(t, "topics_valid_response.json", 200)

	router := setGin()

	controllers.Topics(router)

	req, _ := http.NewRequest("GET", "/topics", nil)
    w := httptest.NewRecorder()
    router.ServeHTTP(w, req)

	responseData, _ := ioutil.ReadAll(w.Body)
	var response models.Topics
	json.Unmarshal(responseData, &response);

	assert.Equal(t, responseCode, w.Code)
	
	responseSchema := getSchema(schemaFile)
	responseLoader := gojsonschema.NewGoLoader(response)

	result, validationError := gojsonschema.Validate(responseSchema, responseLoader)

	if validationError != nil {
        t.Errorf(validationError.Error())
    }

	assert.True(t, result.Valid(), response)
}

func setGin() (*gin.Engine) {

	router := gin.Default()

	return router
}

func getSchema(fileName string) (gojsonschema.JSONLoader) {

	dir, _ := os.Getwd()

	schemaPath := "file://" + dir + "/../../tests_data/schemas/"

	return gojsonschema.NewReferenceLoader(schemaPath + fileName)
}

func mockResponse(t *testing.T, fileName string, statusCode int) {

	dir, _ := os.Getwd()

	filePath := dir + "/../../tests_data/responses/"
	const url = "https://dados.gov.pt/api/1/topics/"

	// httpmock.Activate()
	// defer httpmock.DeactivateAndReset()
  
	httpmock.RegisterResponder("GET", url,
		func(req *http.Request) (*http.Response, error) {
			if req.Header.Get("Accept") != "application/json" {
				t.Errorf("Expected Accept: application/json header, got: %s", req.Header.Get("Accept"))
			}
			resp, err_ := httpmock.NewJsonResponse(statusCode, httpmock.File(filePath + fileName))
			return resp, err_
		},
	)
}