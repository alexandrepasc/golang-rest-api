package api

import (
	"gotest4/restApi/api/controllers"

	"github.com/gin-gonic/gin"
)

func New() (router *gin.Engine) {

	router = buildRouter()

	controllers.Topics(router)
	controllers.Health(router)

	return
}

func Serve(router *gin.Engine) {

	router.Run("localhost:8000")
}

func buildRouter() (*gin.Engine) {

	router := gin.Default()

	return router
}