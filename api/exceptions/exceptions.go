package exceptions

var (
	InternalServerError = "Something went wrong."
	ServiceUnavailable = "The service is unavailable."
)