package api_test

import (
	"encoding/json"
	"gotest4/restApi/api"
	"gotest4/restApi/api/models"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
	"github.com/xeipuuv/gojsonschema"
)

func TestNewFunction(t *testing.T) {

	router := api.New()

	const schemaFile = "topics_valid_response_schema.json"
	const responseCode = 200

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	mockResponse(t, "topics_valid_response.json", 200)

	req, _ := http.NewRequest("GET", "/topics", nil)
    w := httptest.NewRecorder()
    router.ServeHTTP(w, req)

	responseData, _ := ioutil.ReadAll(w.Body)
	var response models.Topics
	json.Unmarshal(responseData, &response);

	assert.Equal(t, responseCode, w.Code)
	
	responseSchema := getSchema(schemaFile)
	responseLoader := gojsonschema.NewGoLoader(response)

	result, validationError := gojsonschema.Validate(responseSchema, responseLoader)

	if validationError != nil {
        t.Errorf(validationError.Error())
    }

	assert.True(t, result.Valid(), response)
}

func TestRunServer(t *testing.T) {

	ts := httptest.NewServer(api.New())

	defer ts.Close()
	
	client := setClient()

	req, _ := http.NewRequest("GET", ts.URL + "/health", nil)
	req.Header.Set("Accept", "application/json")
	resp, errResponse := client.Do(req)

	if (errResponse != nil) {
		t.Errorf(errResponse.Error())
	}
	
	defer resp.Body.Close()

	assert.Equal(t, 200, resp.StatusCode)
}

func TestServe(t *testing.T) {

	router := api.New()

	go api.Serve(router)

	client := setClient()

	req, _ := http.NewRequest("GET", "http://localhost:8000/health", nil)
	req.Header.Set("Accept", "application/json")
	resp, errResponse := client.Do(req)

	if (errResponse != nil) {
		t.Errorf(errResponse.Error())
	}
	
	defer resp.Body.Close()

	assert.Equal(t, 200, resp.StatusCode)
}

func setClient() (http.Client) {

	client := *http.DefaultClient

	return client
}

func getSchema(fileName string) (gojsonschema.JSONLoader) {

	schemaPath := "file://../tests_data/schemas/"

	return gojsonschema.NewReferenceLoader(schemaPath + fileName)
}

func mockResponse(t *testing.T, fileName string, statusCode int) {

	filePath := "../tests_data/responses/"
	const url = "https://dados.gov.pt/api/1/topics/"

	// httpmock.Activate()
	// defer httpmock.DeactivateAndReset()
  
	httpmock.RegisterResponder("GET", url,
		func(req *http.Request) (*http.Response, error) {
			if req.Header.Get("Accept") != "application/json" {
				t.Errorf("Expected Accept: application/json header, got: %s", req.Header.Get("Accept"))
			}
			resp, err_ := httpmock.NewJsonResponse(statusCode, httpmock.File(filePath + fileName))
			return resp, err_
		},
	)
}