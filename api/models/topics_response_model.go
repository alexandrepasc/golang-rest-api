package models

type Topics struct {
	Data []struct {
		CreatedAt    interface{}   `json:"created_at"`
		Datasets     []interface{} `json:"datasets"`
		Deleted      interface{}   `json:"deleted"`
		Description  string        `json:"description"`
		Featured     bool          `json:"featured"`
		ID           string        `json:"id"`
		LastModified interface{}   `json:"last_modified"`
		Name         string        `json:"name"`
		Owner        struct {
			Avatar          interface{} `json:"avatar"`
			AvatarThumbnail interface{} `json:"avatar_thumbnail"`
			Class           string      `json:"class"`
			FirstName       string      `json:"first_name"`
			ID              string      `json:"id"`
			LastName        string      `json:"last_name"`
			Page            string      `json:"page"`
			Slug            string      `json:"slug"`
			URI             string      `json:"uri"`
		} `json:"owner"`
		Page    string        `json:"page"`
		Private bool          `json:"private"`
		Reuses  []interface{} `json:"reuses"`
		Slug    string        `json:"slug"`
		Tags    []string      `json:"tags"`
		URI     string        `json:"uri"`
	} `json:"data"`
	Facets       interface{} `json:"facets"`
	NextPage     interface{} `json:"next_page"`
	Page         int         `json:"page"`
	PageSize     int         `json:"page_size"`
	PreviousPage interface{} `json:"previous_page"`
	Total        int         `json:"total"`
}

type Data struct {
	
}