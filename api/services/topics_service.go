package services

import (
	"encoding/json"
	"fmt"
	"gotest4/restApi/api/models"
	"io/ioutil"
	"net/http"
)

func GetTopics() (response models.Topics, error int) {

	client := setClient()

	req, _ := http.NewRequest("GET", "https://dados.gov.pt/api/1/topics/", nil)
	req.Header.Set("Accept", "application/json")
	// resp, err := client.Get("https://dados.gov.pt/api/1/topics/")
	resp, err := client.Do(req)

	if err != nil {
		return response, 503
	}

	defer resp.Body.Close()

	fmt.Println(resp.Status)

	// if !strings.Contains(resp.Status, "200") {
	if resp.StatusCode != 200 {
		return response, 503
	}

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return response, 500
	}
	
	if err := json.Unmarshal(body, &response); err != nil {   // Parse []byte to go struct pointer
		return response, 500
	}
	// fmt.Println(response)

	return response, 0
}

func setClient() (http.Client) {

	client := *http.DefaultClient

	return client
}