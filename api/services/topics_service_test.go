package services_test

import (
	"gotest4/restApi/api/services"
	"net/http"
	"testing"

	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
	"github.com/xeipuuv/gojsonschema"
)

const url = "https://dados.gov.pt/api/1/topics/"

func TestValidResponse(t *testing.T) {
	
	// httpmock.Activate()
	defer httpmock.DeactivateAndReset()
  
	mockResponse(t, "topics_valid_response.json", 200)
  
	response, err := services.GetTopics()

	schemaLoader := gojsonschema.NewReferenceLoader("file://../../tests_data/schemas/topics_valid_response_schema.json")
	responseLoader := gojsonschema.NewGoLoader(response)

	if err != 0 {
		t.Errorf("Expected '0', got %d", err)
	}

	result, validationError := gojsonschema.Validate(schemaLoader, responseLoader)

	if validationError != nil {
        t.Errorf(validationError.Error())
    }

    if !result.Valid() {
        t.Errorf("The document is not valid. see errors :\n")
        for _, desc := range result.Errors() {
            t.Errorf("- %s\n", desc)
        }
    }
}

func TestRequestInternalServerError(t *testing.T) {

	const schemaFile = "topics_empty_response_schema.json"
	const responseCode = 503

	// httpmock.Activate()
	defer httpmock.DeactivateAndReset()
  
	mockResponse(t, "topics_error_response.json", 500)
  
	response, err := services.GetTopics()

	responseSchema := getSchema(schemaFile)
	responseLoader := gojsonschema.NewGoLoader(response)

	if err != responseCode {
		t.Errorf("Expected '" + string(rune(responseCode)) + "', got %d", err)
	}
	
	result, validationError := gojsonschema.Validate(responseSchema, responseLoader)

	if validationError != nil {
        t.Errorf(validationError.Error())
    }

    if !result.Valid() {
        t.Errorf("The document is not valid. see errors :\n")
        for _, desc := range result.Errors() {
            t.Errorf("- %s\n", desc)
        }
    }
}

func TestRequestConnectionFailure(t *testing.T) {

	const schemaFile = "topics_empty_response_schema.json"
	const responseCode = 503

	// httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	mockConnectionFailure(t)

	response, err := services.GetTopics()

	responseSchema := getSchema(schemaFile)
	responseLoader := gojsonschema.NewGoLoader(response)

	assert.Equal(t, responseCode, err)

	result, validationError := gojsonschema.Validate(responseSchema, responseLoader)

	if validationError != nil {
        t.Errorf(validationError.Error())
    }

    if !result.Valid() {
        t.Errorf("The document is not valid. see errors :\n")
        for _, desc := range result.Errors() {
            t.Errorf("- %s\n", desc)
        }
    }
}

// func TestClient(t *testing.T) {

// 	const schemaFile = "topics_valid_response_schema.json"
// 	const responseCode = 200

// 	httpmock.Activate()
// 	defer httpmock.DeactivateAndReset()
  
// 	mockResponse(t, "topics_valid_response.json", 200)

// 	client := services.setClient()

// 	req, _ := http.NewRequest("GET", url, nil)
// 	req.Header.Set("Accept", "application/json")
// 	resp, err := client.Do(req)

// 	assert.Nil(t, err)

// 	defer resp.Body.Close()

// 	assert.Equal(t, resp.StatusCode, responseCode)

// 	body, _ := ioutil.ReadAll(resp.Body)
// 	var response models.Topics
// 	json.Unmarshal(body, &response);

// 	responseSchema := getSchema(schemaFile)
// 	responseLoader := gojsonschema.NewGoLoader(response)

// 	result, validationError := gojsonschema.Validate(responseSchema, responseLoader)

// 	if validationError != nil {
//         t.Errorf(validationError.Error())
//     }

// 	assert.True(t, result.Valid(), response)
// }

func getSchema(fileName string) (gojsonschema.JSONLoader) {

	schemaPath := "file://../../tests_data/schemas/"

	return gojsonschema.NewReferenceLoader(schemaPath + fileName)
}

func mockResponse(t *testing.T, fileName string, statusCode int) {

	filePath := "../../tests_data/responses/"

	httpmock.Activate()
  
	httpmock.RegisterResponder("GET", url,
		func(req *http.Request) (*http.Response, error) {
			if req.Header.Get("Accept") != "application/json" {
				t.Errorf("Expected Accept: application/json header, got: %s", req.Header.Get("Accept"))
			}
			resp, err := httpmock.NewJsonResponse(statusCode, httpmock.File(filePath + fileName))
			return resp, err
		},
	)
}

func mockConnectionFailure(t *testing.T) {

	httpmock.Activate()
	
	httpmock.RegisterResponder("GET", url,
		func(req *http.Request) (*http.Response, error) {
			if req.Header.Get("Accept") != "application/json" {
				t.Errorf("Expected Accept: application/json header, got: %s", req.Header.Get("Accept"))
			}
			resp, err := httpmock.ConnectionFailure(req)
			return resp, err
		},
	)
}