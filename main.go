package main

import "gotest4/restApi/api"

func main() {

	service := api.New()

	api.Serve(service)
}