package main

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMain(t *testing.T) {

	go main()

	client := setClient()

	req, _ := http.NewRequest("GET", "http://localhost:8000/health", nil)
	req.Header.Set("Accept", "application/json")
	resp, errResponse := client.Do(req)

	if (errResponse != nil) {
		t.Errorf(errResponse.Error())
	}
	
	defer resp.Body.Close()

	assert.Equal(t, 200, resp.StatusCode)
}

func setClient() (http.Client) {

	client := *http.DefaultClient

	return client
}